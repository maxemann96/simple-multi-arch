FROM alpine:3.19

WORKDIR /usr/local/app

RUN apk upgrade --no-cache && \
    apk add --no-cache openjdk21-jre-headless && \
    addgroup -S app && \
    adduser -S -G app app && \
    chown app:app /usr/local/app

USER app

COPY app/build/install/ ./

CMD ["/usr/local/app/app/bin/app"]
